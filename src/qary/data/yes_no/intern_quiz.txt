# state: Welcome

Bot: I'm going to ask you a few questions about python to set a baseline on your python knowledge. This way you can see how much you learn over time. So don't worry if you don't know the answers, you will soon!

# state: Q0, tag: python

Bot: Rate your `python` skill from 0 to 5. Zero if you've never heard of it as a programming language. 5 if you're an internationally recognized expert who regularly writes blog posts, or has written a book about it, or has been the lead developer on a production python app that serves more than 1000 users daily. 3 if you have done any of the following: a) created a github or gitlab repository with some python code that you've written from scratch yourself; b) if you have a reputation of 1000 or more on Stack Exchange, including some Python questions and answers c) if you've ever given a lightning talk about anything python-related at a meetup or python user group.

Player: 3

# state: Q1, tag: python

Bot: Name the two fundamental numerical data types in python? Your answer should be a valid python expression. Bonus points for the shortest possible python expression that produces a tuple that contains the two types with the most general type first.

Player: float, int

# state: Q2, tag: python

Bot: What are the two fundamental data types used to store a sequence of characters?  Your answer should be a valid python expression. Bonus points for the shortest possible python expression that produces a tuple that contains the two types with the most general and flexible type first.

Player: str, bytes

# state: Q3, tag: python

Bot: What are the two fundamental data types used to store an array of objects?  Your answer should be a valid python expression. Bonus points for the shortest possible python expression that produces a tuple that contains the two types with the most general and flexible type first.

Player: list, tuple

# state: Q4, tag: python

Bot: Give me a valid python expression that produces a `list` of the letters of the alphabet from a to t. Bonus points for the shortest possible python expression.

Player: list('abcdefghijklmnopqrst')
Player: ['a', 'b', 'c', 'd', 'e', ... 't']
Player: [chr(i) for i in range(ord('a'), ord('t') + 1)]

# state: Q5, tag: python

Bot: Name one of your Python heros?

Player: Guido Van Rossum

# state: Q6, tag: python

Bot: What is your favorite thing about python?

Player: readability

# state: Q7, tag: prosocial

Bot: Who are your social impact heros?

Player: John Lewis, Martin Luther King, Linus Torvalds, Guido Van Rossum, Richard Stallman in December 2000

# state: Q8, tag

Bot: Who invented Wikipedia?

Player: Richard Stallman in December 2000

