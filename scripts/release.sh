#!/usr/bin/env bash
# release.sh

if [ -z "$1" ] || [ -z "$2" ] ; then
    echo "USAGE: ./scripts/release.sh VERSION MESSAGE"
    echo ""
    echo "EXAMPLE: ./scripts/release.sh 1.2.3 'add new qary skill'"
    exit 1
else
    echo "Recent versions: "
    git tag | sort | tail -n 10
    echo ""
    echo "Tagging the git repository with git tag -a '$1' -m '$2'..."
    echo "Do you want to proceed [N]/y?"
fi

read answer

if [ "$answer" != "y" ] ; then
    exit 0
fi

set -e
# pip install -U twine wheel setuptools
git commit -am "$1: $2"
git push
git tag -l | cat
git tag -a "$1" -m "$2"

rm -rf build
rm -rf dist

echo "Large data files..."
find src/qary/data -type f -size +20M -exec ls -hal {} \;
echo ""
echo "Would you like to remove these files?"
read answer
if [ "$answer" == "y" ] ; then
    find src/qary/data -type f -size +20M -exec rm -f {} \;
fi

# python setup.py sdist
# python setup.py bdist_wheel

# if [ -z "$(which twine)" ] ; then
#     echo 'Unable to find `twine` so installing it with pip.'
#     pip install --upgrade pip setuptools twine poetry
# fi


sed -i 's/^version\s*=\s*"[0-9]*\.[0-9]*\.[0-9]*"/version = "'$1'"/g' scripts/pyproject.toml
grep '^version' scripts/pyproject.toml
git commit -am "$1: $2"
cp scripts/pyproject.toml .
poetry build
poetry publish

rm pyproject.toml

git push --tag
