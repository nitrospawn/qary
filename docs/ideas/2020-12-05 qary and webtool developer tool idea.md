# qary and webtool developer tool idea

Recommend which stack exchange to ask your question based on NLP of rejected questions and accepted questions.

Query one or more stackexchanges to find the answer or similar questions that are popular so you can reword your question.

Suggest rewording to comply with that stackexchange question rules and style guide.

Autoformat code blocks etc.
