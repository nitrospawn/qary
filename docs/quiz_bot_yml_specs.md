> Note: To see this rendered in a more easy to read form, open this in an application that can render markdown files or an editor that can support markdown previews. Editors like MS VSCode had Markdown preview plugins

# Notes on the yaml format supported in qary with a special emphasis on the quiz bot skill

The examples illustrate the yml files that would be used to indicate a python language quiz. 

## 1. Simple, linear next state
This is the simplest form for a statemachine where one state directly leads to the next sequential state. 
- the 'next' state does not need to be specified and will by default move to the next sequential state, regardless of player response
### State transition diagram
Each box in the diagram below indicates a state. Q0, Q1 etc. indicate different questions in the quiz
```mermaid
graph TD;
    welcome_state --> Q1; 
    Q1 --> Q2;
    Q2 --> __finish__;
```
### Equivalent yaml file
```yml
-
  state: __welcome__
  Bot: 
    - Welcome
    - I will ask you a few Python questions. Please answer them
-
  state: Q1
  Bot: 
    - Name the two fundamental numerical data types in Python
-
  state: Q2
  Bot: 
    - Name the fundamental Python data structure container type that can only contain unique items 
-
  state: __finish__
  Bot: 
    - Goodbye
-
```

## 2. State that could move to one of two possible states depending on user input
Assume that during the welcome_state the player has a choice to whether go through with the quiz during the welcome state. 

A  `next` and a  `next_condition` keys are now introduced. The `next` key is the default state that will be transitioned to if the player response does not match any of the statements in the `next_condition` values

If only the `next_condition` key is present and the player response does not match any of the responses listed under there, then the default `next` state is assumed to be the next sequential state following the current one.

### State transition diagram
Each box in the diagram below indicates a state. Q0, Q1 etc. indicate different questions in the quiz
```mermaid
graph TD;
    welcome_state -- yes or ok --> Q1 ;
    welcome_state -- all other responses--> __finish__ ;
    Q1 --> Q2;
    Q2 --> __finish__;
```
### Equivalent yaml file
```yml
-
  state: welcome_state
  Bot: 
    - Welcome
    - I will ask you a few Python questions. Please answer 'yes' if you would like to continue
  next: __finish__  # state to go to if 'yes' or 'ok' are not the p;ayer responses
  next_condition:
    Q1:   # state to go to if 'yes' or 'ok' are entered.
      - 'yes'
      - ok
-
  state: Q1
  Bot: 
    - Name the two fundamental numerical data types in Python
-
  state: Q2
  Bot: 
    - Name the fundamental Python data structure container type that can only contain unique items 
-
  state: __finish__
  Bot: 
    - Goodbye
-
```

# Other notes of interest such as case sensitivity
1. The entry state (often referred to as the `welcome` state)is the very first state that is listed in the yml file and the exist state is the very last one 
2. The keywords in the yaml file (the `keys`) such as `state`, `bot` etc. are case *insensitive* and will be normalized in the code
3. The state names listed are case *sensitive*. 
    - `q0` and `Q0` states in the yaml file will be treated as two distinct unique states
4. The player responses are normalized within the code and case and punctuation differences will be ignored 
